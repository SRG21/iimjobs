
import React, { useState, useEffect, useRef, useCallback } from "react";

function App() {
  //Change the value of max width and step-size to change the no. of concentric squares formed
  const [state, setSate] = useState({max_width: 1000, step_size: 200, top: 20, left:20});
  const [square, setSquare] = useState([{width: 400, top: 20, left:20}]);
  const [isSquare, setIsSquare] = useState(true);

  const drawSquare = () => {
    let arr = new Array();
    let edge = state.max_width;
    let step = state.step_size;
    let top = state.top;
    let left = state.left;
    
    while(edge >= step){
      let obj = new Object({});
      obj = {...obj, width: edge, top: top, left: left}
      arr.push(obj);
      edge = edge - step;
      top = top + step/2;
      left = left + step/2;
    }

    if(edge < step){
      setSquare(arr);
      setIsSquare(false);
    }
  }

  useEffect(()=> {
    console.log(square);
    if(isSquare){
      drawSquare();
    } 
  },[isSquare]);


  return (
    <div className="App">
      {
        square.map((sq, i) => (
          <div
            id = {i+1}
            style = {
              {
                position: "absolute",
                border: "2px solid black",
                width: sq.width,
                height: sq.width,
                top: sq.top,
                left: sq.left 
              }
            }
            
          >
          </div>
        ))
      }
    </div>
  );
}

export default App;
